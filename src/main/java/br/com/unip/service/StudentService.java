package br.com.unip.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.unip.domain.Student;

@Service
public class StudentService {

	private List<Student> students = new ArrayList<>();
	
	public List<Student> findAllStudents() {
		
		return this.students;
	}

	public void createStudent(Student student) {
		
		this.students.add(student);
	}
	
}
