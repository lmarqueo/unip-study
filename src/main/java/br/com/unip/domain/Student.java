package br.com.unip.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class Student implements Serializable {
	private static final long serialVersionUID = 7895546414194389994L;

	private String ra;
	
	private String name;
	
}
